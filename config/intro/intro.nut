/****************************************
*
*  intro Layout
*  author: Keil Miller Jr
*
****************************************/

fe.load_module("common")
fe.load_module("file")

function on_exit() { fe.signal("select") }

/****************************************
*
*  User Config
*
****************************************/

local order = 0

class UserConfig {
	</ label = "State", help =" Toggle state of Intro when Attract-Mode starts", options = "On,Off", order = order++ />
	state = "On"

	</ label = "Media Path", help = "Path to valid media file or directory.", order = order++ />
	media_path = "4x3/"

	</ label = "Image Duration", help = "Duration (ms) to show images.", order = order++ />
	image_duration = "5000"

	</ label = "Force 4:3 aspect", help = "Force 4:3 aspect ratio.", options = "Yes,No", order = order++ />
	force_aspect = "No"

	</ label = "CRT Shader", help = "CRT Shader applied.", options = "Disabled,Crt Cgwg,Crt Lottes", order = order++ />
	crt_shader = "Disabled"

	</ label = "Bloom Shader", help = "Bloom applied with CRT shaders.", options = "Yes,No", order = order++ />
	bloom_shader = "No"
}

local user_config = fe.get_config()

/****************************************
*  state
****************************************/

user_config.state = to_boolean(user_config.state);
if (!user_config.state) on_exit()

/****************************************
*  media path
****************************************/

// Expand if relative
if (fe.path_test(user_config.media_path, PathTest.IsRelativePath)) user_config.media_path = fe.script_dir + user_config.media_path

// Test if valid
try { assert((fe.path_test(user_config.media_path, PathTest.IsFile) && fe.path_test(user_config.media_path, PathTest.IsSupportedMedia)) || fe.path_test(user_config["media_path"], PathTest.IsDirectory)) }
catch (e) {
	log.warn("You must have a valid media path to supported media for Intro to work. Skipping Intro.")
	on_exit()
}

// Media path is directory
if (fe.path_test(user_config.media_path, PathTest.IsDirectory)) {
	local files = [];
	local directory = DirectoryListing(user_config.media_path, true)

	// Store filename if valid media
	for (local i = 0; i < directory.results.len(); i++) {
		if (fe.path_test(directory.results[i], PathTest.IsFile) && fe.path_test(directory.results[i], PathTest.IsSupportedMedia)) files.push(directory.results[i])
	}

	// Set to random file
	try {
		assert(files.len() > 0)
		user_config.media_path = files[random_integer(files.len() - 1)]
	}
	// Directory is empty, warn and exit
	catch (e) {
		log.warn("Your media path has no supported media. Skipping Intro.");
		on_exit();
	}
}

/****************************************
*  image duration
****************************************/

try {
	assert(user_config.image_duration = user_config.image_duration.tointeger())
	assert(user_config.image_duration >= 0)
}
catch (e) {
	log.warn("You must have an integer value for the Intro's image duration. Falling back to default.")
	user_config.image_duration = user_config.image_duration.tointeger()
}

/****************************************
*  force aspect
****************************************/

user_config.force_aspect = to_boolean(user_config.force_aspect)

/****************************************
*  crt shader
****************************************/

try { explode(UserConfig.getattributes("crt_shader").options).find(user_config.crt_shader) != null }
catch (e) {
	log.warn("You must have a valid option for the Intro's CRT Shader. Falling back to default.")
	user_config.crt_shader = explode(UserConfig.getattributes("crt_shader").options)[0]
}

/****************************************
*  bloom shader
****************************************/

user_config.bloom_shader = to_boolean(user_config.bloom_shader)

/****************************************
*
*  Config
*
****************************************/

local config = {}, flh = fe.layout.height, flw = fe.layout.width

config.window <- {
	height = flh,
	width = to_boolean(user_config.force_aspect) ? match_aspect({aspect_height = 3, aspect_width = 4, height = flh}) : flw,
	x = to_boolean(user_config.force_aspect) ? (flw - match_aspect({aspect_height = 3, aspect_width = 4, height = flh})) / 2 : 0,
	y = 0,
}

config.container <- {
	height = flh,
	width = to_boolean(user_config.force_aspect) ? match_aspect({aspect_height = 3, aspect_width = 4, height = flh}) : flw,
	x = 0,
	y = 0,
}

config.media <- {
	height = config.container.height,
	width = config.container.width,
	x = 0,
	y = 0,
  video_flags = Vid.NoLoop,
}

/****************************************
*
*  Objects
*
****************************************/

// Draw surfaces to fe in reverse order
local container = fe.add_surface(config.container.width, config.container.height)
local window = fe.add_surface(config.window.width, config.window.height)

// Reassign reference pointer to clone with appropriate parent
container.visible = false
container = window.add_clone(container)
container.visible = true

set_properties(container, config.container)
set_properties(window, config.window)

local media = container.add_image(user_config.media_path, -1, -1, 1, 1)
set_properties(media, config.media)

/****************************************
*
*  Callbacks and Handlers
*
****************************************/

function duration(ttime) {
	// Video or image is done
  if ((media.video_duration > 0 && media.video_playing == false) || (media.video_duration == 0 && ttime > user_config.image_duration)) on_exit()
  return false
}

fe.add_ticks_callback("duration");

/****************************************
*
*  Shaders
*
****************************************/

if (user_config.crt_shader != "Disabled") {
	local bloom = Bloom()
	window.shader = bloom.shader
}

switch (user_config.crt_shader) {
	case "Crt Lottes":
		local crt_lottes = CrtLottes()
		container.shader = crt_lottes.shader
		break;
	case "Crt Cgwg":
		local crt_cgwg = CrtCgwg()
		container.shader = crt_cgwg.shader
		break;
}
