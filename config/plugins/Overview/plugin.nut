/****************************************
*
*  Overview Plugin
*  author: Keil Miller Jr
*
****************************************/

fe.load_module("common")
fe.load_module("file")
fe.load_module("submenu")

/****************************************
*
*  User Config
*
****************************************/

local order = 0

class UserConfig {
	</ label = "Control", help = "Control to show or hide overview", options = "custom1,custom2,custom3,custom4,custom5,custom6,Disabled", order = order++ />
	control = "Disabled"

	</ label = "Rows", help = "Set the number of rows of text to display in the viewer", order = order++ />
	rows = "30"

	</ label = "Force 4:3 aspect", help = "Force 4:3 aspect ratio.", options = "Yes,No", order = order++ />
	force_aspect = "No"

	</ label = "History.dat Path", help = "Path to History.dat file.", order = order++ />
	dat_path = "$HOME/History.dat"

	</ label = "Generate", help = "Generate overview files.", is_function = true, order = order++ />
	generate = "generate_overviews"
}

/****************************************
*
*  Generator
*
****************************************/

class OverviewGenerator {
	ROMLIST_PATH = FeConfigDirectory + "romlists/mame.txt"
	WRITE_PATH = FeConfigDirectory + "scraper/mame/overview/"
	_overviews = null

	constructor(dat_path) {
		this._overviews = {}

		add_keys()
		add_values(fe.path_expand(dat_path))
		if (this._overviews.len() > 0) write()
		else { log.warn("No overviews were generated.") }
	}

	function add_keys() {
		local lines = read_file(ROMLIST_PATH)

		if (!lines) return
		for (local current_progress, eob, i = 0, progress = 0; i < lines.len(); i++) {
			current_progress = percentage({part = i + 1, whole = lines.len()}).tointeger()
			if (progress < current_progress) {
				progress = current_progress
				fe.overlay.splash_message("Adding keys to overviews (" + progress + "%)")
			}

			if (lines[i].find("#") != null) continue // Skip header

			eob = lines[i].find(";") // Find position of ;, returns null if not found
			if (eob != null && eob > 0) { // If eob is found and not at the beginning
				this._overviews[lines[i].slice(0, eob)] <- ""
			}
		}
	}

	function add_values(path) {
		local lines = read_file(path)

		if (!lines) return

		for (local current_progress, eq, i = 0, progress = 0, rom, match; i < lines.len(); i++) {
			current_progress = percentage({part = i + 1, whole = lines.len()}).tointeger()
			if (progress < current_progress) {
				progress = current_progress
				fe.overlay.splash_message("Adding values to overviews (" + progress + "%)")
			}

			if (lines[i].find("##") != null || lines[i].len() < 5) continue // Skip comments, $bio, $end, empty line

			eq = lines[i].find("=", 1) // Find position of =, returns null if not found
			if (lines[i].slice(0,1) == "$" && eq != null) { // If line starts with $ and eq is found
				rom = split(lines[i].slice(eq + 1), ",")[0]

				if (rom in this._overviews) match = true
				else match = false
			}
			else if (rom && match) { // Must have illiterated over a rom name and match is true
				this._overviews[rom] += lines[i] + "\n" // Append line
			}
		}
	}

	function read_file(path) {
		if (!fe.path_test(path, PathTest.IsFile)) return

		local f = ReadTextFile(path), lines = []

		local filename = split(path, "/")
		filename = filename[filename.len() - 1]

		for (local current_progress, progress = 0; !f.eos();) {
			current_progress = percentage({part = f._f.tell(), whole = f._f.len()}).tointeger()
			if (progress < current_progress) {
				progress = current_progress
				fe.overlay.splash_message("Reading " + filename + " (" + progress + "%)")
			}

      lines.push(strip(f.read_line()))
    }

		return lines
	}

	function write() {
		system( "mkdir \"" + WRITE_PATH + "\"" ); // Create directory if not exist

		local i = 0, progress = 0
		foreach (rom, overview in this._overviews) {
			local current_progress = percentage({part = i + 1, whole = this._overviews.len()}).tointeger()
			if (progress < current_progress) {
				progress = current_progress
				fe.overlay.splash_message("Writing Files (" + progress + "%)")
			}

			local file_path = WRITE_PATH + rom + ".txt"
			if (!fe.path_test(file_path, PathTest.IsFile)) { // Create file if not exist
				local f = WriteTextFile(file_path)
				f.write_line(overview)
			}

			i++
		}
	}
}

function generate_overviews(user_config) {
	local overview_generator = OverviewGenerator(user_config.dat_path)
	return "Finished generating overview files."
}

/****************************************
*
*  Local Variables
*
****************************************/

local user_config = fe.get_config()

/****************************************
*
*  Viewer
*
****************************************/

class OverviewViewer extends SubMenu {
	static CONFIG = {}
	static FLH = fe.layout.height
	static FLW = fe.layout.width
	_container = null
	_text = null
	_window = null

	constructor() {
		/****************************************
		* Static Variables
		****************************************/

		if (CONFIG.len() == 0) {
			CONFIG.window <- {
				height = FLH,
				width = to_boolean(user_config.force_aspect) ? match_aspect({aspect_height = 3, aspect_width = 4, height = FLH}) : FLW,
				x = to_boolean(user_config.force_aspect) ? (FLW - match_aspect({aspect_height = 3, aspect_width = 4, height = FLH})) / 2 : 0,
				y = 0,
			}

			CONFIG.container <- {
				x = 0,
				y = 0,
				width = to_boolean(user_config.force_aspect) ? match_aspect({aspect_height = 3, aspect_width = 4, height = FLH}) : FLW,
				height = FLH,
			}

			CONFIG.text <- {
				bg_alpha = 220,
				charsize = fe.layout.height / user_config.rows.tointeger(),
				first_line_hint = 0, // enables word wrapping
				height = CONFIG.container.height,
				width = CONFIG.container.width,
				x = 0,
				y = 0,
			}
		}

		/****************************************
		* Create Instance Objects
		****************************************/

		// Draw surfaces to fe in reverse order
		_container = fe.add_surface(CONFIG.container.width, CONFIG.container.height)
		_window = fe.add_surface(CONFIG.window.width, CONFIG.window.height)

		// Reassign reference pointer to clone with appropriate parent
		_container.visible = false
		_container = _window.add_clone(_container)
		_container.visible = true

		set_properties(_container, CONFIG.container)
		set_properties(_window, CONFIG.window)

		_text = _container.add_text("", -1, -1, 1, 1)
		set_properties(_text, CONFIG.text)

		/****************************************
		* Logic
		****************************************/

		base.constructor(user_config.control)

		on_hide()
	}

	function on_hide() { _text.visible = false }

	function on_scroll_down() { _text.first_line_hint++ }

	function on_scroll_up() { _text.first_line_hint-- }

	function on_show() {
		_text.msg = fe.game_info(Info.Overview)
		_text.visible = true
	}
}

if (user_config.control != "Disabled") fe.plugin["Overview"] <- OverviewViewer()
