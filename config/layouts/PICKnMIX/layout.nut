/****************************************
*
*  PICKnMIX Layout
*  author: Keil Miller Jr
*  mentions: [The Universe Bios](http://unibios.free.fr)
*
*  Suggested key config:
*
*  Button         | Action                    | Key
*  -----------------------------------------------------
*  1 Player Start | select                    | 1
*  Select         | back                      | Escape
*  Up             | prev_game                 | Up
*  Down           | next_game                 | Down
*  Left           | prev_letter               | Left
*  Right          | next_letter               | Right
*  A              | add_favourite             | LControl
*  B              | custom1 (Overview plugin) | LAlt
*  C              | next_filter               | Space
*  D              | next_display              | LShift
*
****************************************/

fe.load_module("common")
fe.load_module("fade")
fe.load_module("preserve-art")
fe.do_nut("cycle.nut")
fe.do_nut("osd.nut")

/****************************************
*
*  User Config
*
****************************************/

local order = 0

class UserConfig {
	</ label = "Format Game Title", help = "Remove parenthesis, slashes, brackets and following text from game title.", options = "Yes, No", order = order++ />
	format_title = "Yes"

	</ label = "Force 4:3 aspect", help = "Force 4:3 aspect ratio.", options = "Yes, No", order = order++ />
	force_aspect = "No"

	</ label = "Media Label", help = "Label that identifies the background media artwork.", order = order++ />
	media_label = "snap"

	</ label = "Media Audio", help = "Audio for media.", options = "On, Off", order = order++ />
	media_audio = "On"

	</ label = "Logo Artwork Label", help = "Label that identifies the logo artwork displayed over the media artwork.", order = order++ />
	logo_label = "wheel"

	</ label = "OSD Color", help = "Color of the OSD border and favorite rom.", options = "Blue, Bronze, Green, Yellow", order = order++ />
	osd_color = "Bronze"

	</ label = "OSD Effect Type", help = "Effect type when the OSD disappears.", options = "Collapse, Compress, Disappear, Fade, Roll Up, Roll Down", order = order++ />
	osd_effect_type = "Collapse"

	</ label = "OSD Effect Delay", help = "Delay (ms) when the OSD disappears.", order = order++ />
	osd_effect_delay = 2000

	</ label = "OSD Effect Duration", help = "Effect duration (ms) when the OSD disappears.", order = order++ />
	osd_effect_duration = 200

	</ label = "Cycle Enabled", help = "Enable Cycle.", options = "Yes, No", order = order++ />
	cycle_enabled = "Yes"

	</ label = "Cycle Delay", help = "Delay (ms) when a new game is selected. Video length takes precedence.", order = order++ />
	cycle_delay = "60000"

	</ label = "CRT Shader", help = "CRT Shader applied.", options = "Disabled, Crt Cgwg, Crt Lottes", order = order++ />
	crt_shader = "Disabled"

	</ label = "Bloom Shader", help = "Bloom applied with CRT shaders.", options = "Yes, No", order = order++ />
	bloom_shader = "No"
}

local user_config = fe.get_config()

/****************************************
*  Format Title
****************************************/

user_config.format_title = to_boolean(user_config.format_title)

/****************************************
*  Force Aspect
****************************************/

user_config.force_aspect = to_boolean(user_config.force_aspect)

/****************************************
*  Media Audio
****************************************/

user_config.media_audio = to_boolean(user_config.media_audio)

/****************************************
*  OSD Color
****************************************/

try { explode(UserConfig.getattributes("osd_color").options).find(user_config.osd_color) != null }
catch (e) {
	log.warn("You must have a valid option for the PICKnMIX's OSD Color. Falling back to default.")
	user_config.osd_color = explode(UserConfig.getattributes("osd_color").options)[1]
}

/****************************************
*  OSD Effect Type
****************************************/

try { explode(UserConfig.getattributes("osd_effect_type").options).find(user_config.osd_effect_type) != null }
catch (e) {
	log.warn("You must have a valid option for the PICKnMIX's OSD Effect Type. Falling back to default.")
	user_config.osd_effect_type = explode(UserConfig.getattributes("osd_effect_type").options)[0]
}

/****************************************
*  OSD Effect Delay
****************************************/

try {
	assert(user_config.osd_effect_delay = user_config.osd_effect_delay.tointeger())
	assert(user_config.osd_effect_delay >= 0)
}
catch (e) {
	log.warn("You must have a valid option for the PICKnMIX's OSD Effect Delay. Falling back to default.")
	user_config.osd_effect_delay = user_config.osd_effect_delay.tointeger()
}

/****************************************
*  Cycle Enabled
****************************************/

user_config.cycle_enabled = to_boolean(user_config.cycle_enabled)

/****************************************
*  Cycle Delay
****************************************/

try {
	assert(user_config.cycle_delay = user_config.cycle_delay.tointeger())
	assert(user_config.cycle_delay >=0 )
}
catch (e) {
	log.warn("You must have a valid option for the PICKnMIX's OSD Cycle Delay. Falling back to default.")
	user_config.cycle_delay = user_config.cycle_delay.tointeger()
}

/****************************************
*  CRT Shader
****************************************/

try { explode(UserConfig.getattributes("crt_shader").options).find(user_config.crt_shader) != null }
catch (e) {
	log.warn("You must have a valid option for the PICKnMIX's CRT Shader. Falling back to default.")
	user_config.crt_shader = explode(UserConfig.getattributes("crt_shader").options)[0]
}

/****************************************
*  Bloom Shader
****************************************/

user_config.bloom_shader = to_boolean(user_config.bloom_shader)

/****************************************
*
*  Config
*
****************************************/

fe.layout.font = "roboto_mono_bold"

local config = {}, flh = fe.layout.height, flw = fe.layout.width

config.test_text <- { margin = 0 }

config.window <- {
	height = flh,
	width = to_boolean(user_config.force_aspect) ? match_aspect({aspect_height = 3, aspect_width = 4, height = flh}) : flw,
	x = to_boolean(user_config.force_aspect) ? (flw - match_aspect({aspect_height = 3, aspect_width = 4, height = flh})) / 2 : 0,
	y = 0,
}

config.container <- {
	height = flh,
	width = to_boolean(user_config.force_aspect) ? match_aspect({aspect_height = 3, aspect_width = 4, height = flh}) : flw,
	x = 0,
	y = 0,
}

config.media <- {
	height = config.container.height,
	trigger = Transition.EndNavigation,
	width = config.container.width,
	x = 0,
	y = 0,
}
if (!to_boolean(user_config.media_audio)) config.media.video_flags <- Vid.NoAudio

config.logo <- {
	height = percentage({percent = 20, whole = config.container.height}),
	padding = percentage({percent = 4, whole = config.container.width}),
	preserve_aspect_ratio = true,
	trigger = Transition.EndNavigation,
}
config.logo.width <- match_aspect({aspect_height = 7, aspect_width = 16, height = config.logo.height}) // Aspect based on average size of 400x175
config.logo.x <- config.container.width - config.logo.padding * 1.5 - config.logo.width
config.logo.y <- config.container.height - config.logo.padding - config.logo.height

config.osd <- {}

config.osd.color <- {
	blue = [66, 98, 174],
	bronze = [159, 143, 99],
	green = [22, 158, 25],
	yellow = [224, 244, 0],
}
try {
	assert(config.osd.color.rawin(strip(user_config.osd_color)) == true)
	config.osd.color = config.osd.color.rawget(strip(user_config.osd_color))
}
catch  (e) {
	log.warn("Improper color for PICKnMIX's OSD, switching to default value.")
	config.osd.color = config.osd.color.yellow
}

config.osd.surface <- {
	height = percentage({percent = 64, whole = config.container.height}),
	width = percentage({percent = 76, whole = config.container.width}),
}
config.osd.surface.x <- config.container.width / 2 - config.osd.surface.width / 2
config.osd.surface.y <- config.container.height / 2 - config.osd.surface.height / 2

config.osd.border <- {
	file_name = resources_path + "pixel.png",
	height = config.osd.surface.height,
	rgb = config.osd.color,
	width = config.osd.surface.width,
	x = 0,
	y = 0,
}

config.osd.background <- {
	file_name = resources_path + "pixel.png",
	height = config.osd.surface.height - percentage({percent = 2, whole = config.osd.surface.width}),
	rgb = [16, 16, 16],
	width = percentage({percent = 98, whole = config.osd.surface.width}),
	x = percentage({percent = 1, whole = config.osd.surface.width}),
	y = percentage({percent = 1, whole = config.osd.surface.width}),
}

config.osd.rows <- { padding = percentage({percent = 3, whole = config.osd.surface.width}) }
config.osd.rows.height <- (config.osd.surface.height - config.osd.rows.padding * 2) / 14
config.osd.rows.width <- config.osd.surface.width - config.osd.rows.padding * 2

config.osd.topLine <- {
	file_name = resources_path + "pixel.png",
	height = percentage({percent = 0.66, whole = config.osd.surface.width}),
	rgb = [224, 224, 224],
	width = config.osd.rows.width,
	x = config.osd.rows.padding,
}
config.osd.topLine.y <- config.osd.rows.padding + config.osd.rows.height * 5.5-config.osd.topLine.height / 2,

config.osd.bottomLine <- {
	file_name = resources_path + "pixel.png",
	height = percentage({percent = 0.66, whole = config.osd.surface.width}),
	rgb = [224, 224, 224],
	width = config.osd.rows.width,
	x = config.osd.rows.padding,
}
config.osd.bottomLine.y <- config.osd.rows.padding + config.osd.rows.height * 8.5 - config.osd.bottomLine.height / 2,

config.osd.title <- []
	for (local index = 0; index < 11; index++) { config.osd.title.push({}) }
	foreach (index, item in config.osd.title) {
		item.align <- Align.MiddleCentre
		item.width <- config.osd.rows.width
		item.x <- config.osd.rows.padding

		index != 5 ? item.height <- config.osd.rows.height : item.height <- config.osd.rows.height * 2

		switch (index) {
			case 0:
				item.height <- config.osd.rows.height
				item.rgb <- [40, 40, 40]
				item.y <- config.osd.rows.padding
				break
			case 1:
				item.bg_rgb <- [24, 24, 24]
				item.rgb <- [56, 56, 56]
				item.y <- config.osd.rows.padding + config.osd.rows.height
				break
			case 2:
				item.bg_rgb <- [32, 32, 32]
				item.rgb <- [80, 80, 80]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 2
				break
			case 3:
				item.bg_rgb <- [40, 40, 40]
				item.rgb <- [96, 96, 96]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 3
				break
			case 4:
				item.bg_rgb <- [40, 40, 40]
				item.rgb <- [96, 96, 96]
				item.y <- config.osd.rows.padding + config.osd.rows.height *4 
				break
			case 5:
				item.rgb <- [224, 224, 224]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 6
				break
			case 6:
				item.bg_rgb <- [40, 40, 40]
				item.rgb <- [96, 96, 96]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 9
				break
			case 7:
				item.bg_rgb <- [40, 40, 40]
				item.rgb <- [96, 96, 96]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 10
				break
			case 8:
				item.bg_rgb <- [32, 32, 32]
				item.rgb <- [80, 80, 80]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 11
				break
			case 9:
				item.bg_rgb <- [24, 24, 24]
				item.rgb <- [56, 56, 56]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 12
				break
			case 10:
				item.rgb <- [40, 40, 40]
				item.y <- config.osd.rows.padding + config.osd.rows.height * 13
				break
		}
	}

config.osd.platform<- {
	align = Align.TopLeft,
	bg_rgb = [16, 16, 16],
	height = config.osd.rows.height,
	margin = 0,
	rgb = [224, 224, 224],
	width = config.osd.rows.width,
	x = config.osd.rows.padding,
	y = config.osd.rows.padding,
}

config.osd.filter <- {
	align = Align.BottomRight,
	bg_rgb = [16, 16, 16],
	height = config.osd.rows.height,
	margin = 0,
	msg = "[FilterName]",
	rgb = [224, 224, 224],
	width = config.osd.rows.width,
	x = config.osd.rows.padding,
	y = config.osd.rows.padding+config.osd.rows.height*13,
}

/****************************************
*
*  Objects
*
****************************************/

function format_title(index_offset=0) {
	if (to_boolean(user_config.format_title) && (fe.game_info(Info.Title, index_offset) != "")) {
		local s = split(fe.game_info(Info.Title, index_offset), "(/[")
		return rstrip(s[0]).toupper()
	}
	else return fe.game_info(Info.Title, index_offset).toupper()
}

function format_platform() {
	if (fe.list.display_index < 0) return "PICKnMIX"
	return fe.list.name.toupper()
}

local test_text = fe.add_text("", 0, flh, flw, 0)
	set_properties(test_text, config.test_text)

// Draw surfaces to fe in reverse order
local container = fe.add_surface(config.container.width, config.container.height)
local window = fe.add_surface(config.window.width, config.window.height)

// Reassign reference pointer to clone with appropriate parent
container.visible = false
container = window.add_clone(container)
container.visible = true

set_properties(container, config.container)
set_properties(window, config.window)

local media = container.add_artwork(user_config.media_label, -1, -1, 1, 1)
set_properties(media, config.media)

local logo = PreserveArt(user_config.logo_label, config.logo.x, config.logo.y, config.logo.width, config.logo.height, container)
logo.trigger = Transition.EndNavigation
logo.set_fit_or_fill("fit")
logo.set_anchor(::Anchor.Bottom)

local osd = {}

osd.surface <- container.add_surface(config.osd.surface.width, config.osd.surface.height)
set_properties(osd.surface, config.osd.surface)

osd.border <- osd.surface.add_image(resources_path + "pixel.png")
set_properties(osd.border, config.osd.border)

osd.background <- osd.surface.add_image(resources_path + "pixel.png")
set_properties(osd.background, config.osd.background)

osd.topLine <- osd.surface.add_image(resources_path + "pixel.png")
set_properties(osd.topLine, config.osd.topLine)

osd.bottomLine <- osd.surface.add_image(resources_path + "pixel.png")
set_properties(osd.bottomLine, config.osd.bottomLine)

osd.title <- []
for (local index = 0; index < 11; index++) {
	osd.title.push(osd.surface.add_text("", -1, -1, 1, 1))
	set_properties(osd.title[index], config.osd.title[index])
}

osd.platform <- osd.surface.add_text("", -1, -1, 1, 1)
set_properties(osd.platform, config.osd.platform)

osd.filter <- osd.surface.add_text("", -1, -1, 1, 1)
set_properties(osd.filter, config.osd.filter)

/****************************************
*
*  Callbacks and Handlers
*
****************************************/

function on_signal(signal_str) {
	switch (signal_str) {
		case "up":
			fe.signal("prev_game")
			return true
		case "down":
			fe.signal("next_game");
			return true
		case "left":
			fe.signal("prev_letter")
			return true
		case "right":
			fe.signal("next_letter")
			return true
	}

	return false;
}

fe.add_signal_handler("on_signal")

function on_transition(ttype, var, ttime) {
	switch (ttype) {
		case Transition.ToNewList:
			// Platform
			osd.platform.msg = format_platform()
			test_text.height = config.osd.platform.height
			test_text.msg = osd.platform.msg
			local osdPlatformCharWidth = test_text.msg_width
			if (osdPlatformCharWidth < config.osd.platform.width) { osd.platform.width = osdPlatformCharWidth }
			else osd.platform.width = config.osd.platform.width

			// Filter
			test_text.height = config.osd.filter.height
			test_text.msg = osd.filter.msg
			local osd_filter_char_width = test_text.msg_width
			if (osd_filter_char_width < config.osd.filter.width) {
				osd.filter.width = osd_filter_char_width
				osd.filter.x = config.osd.filter.x + config.osd.filter.width - osd.filter.width
			}
			else {
				osd.filter.width = config.osd.filter.width
				osd.filter.x = config.osd.filter.x
			}
			break
	}

	switch (ttype) {
		case Transition.ToNewSelection:
		case Transition.FromOldSelection:
		case Transition.ToNewList:
		case Transition.ChangedTag:
			osd.title[0].msg = format_title(-5)
			osd.title[1].msg = format_title(-4)
			osd.title[2].msg = format_title(-3)
			osd.title[3].msg = format_title(-2)
			osd.title[4].msg = format_title(-1)
			osd.title[5].msg = format_title()
			osd.title[6].msg = format_title(1)
			osd.title[7].msg = format_title(2)
			osd.title[8].msg = format_title(3)
			osd.title[9].msg = format_title(4)
			osd.title[10].msg = format_title(5)

			fe.game_info(Info.Favourite) == "1" ? osd.title[5].set_rgb(config.osd.color[0] * 0.8, config.osd.color[1] * 0.8, config.osd.color[2] * 0.8) : osd.title[5].set_rgb(224, 224, 224)
			break
	}
	return false
}

fe.add_transition_callback("on_transition")

/****************************************
*
*  Shaders
*
****************************************/

if (user_config.crt_shader != "Disabled") {
	local bloom = Bloom()
	window.shader = bloom.shader
}

switch (user_config.crt_shader) {
	case "Crt Lottes":
		local crt_lottes = CrtLottes()
		container.shader = crt_lottes.shader
		break;
	case "Crt Cgwg":
		local crt_cgwg = CrtCgwg()
		container.shader = crt_cgwg.shader
		break;
}

/****************************************
*
*  Class Instances
*
****************************************/

if (to_boolean(user_config.cycle_enabled)) local cycle = Cycle(media, user_config.cycle_delay)

local osd = OSD(osd.surface, user_config.osd_effect_type, user_config.osd_effect_delay, user_config.osd_effect_duration)
