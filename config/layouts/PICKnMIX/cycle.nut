/****************************************
*
*  Cycle
*  author: Keil Miller Jr
*
****************************************/

class Cycle {
	delay = null
	object = null
	transition_time = 0

	constructor(obj, de = 60000) {
		// Object
		object = obj

		// Delay
		try {
			delay = de.tointeger()
			assert(delay >= 1)
		}
		catch(e) {
			log.warn("Improper cycle delay time, switching to default value.")
			delay = 60000
		}

		fe.add_ticks_callback(this, "on_tick")
		fe.add_transition_callback(this, "on_transition")
	}

	function next() { fe.list.index = random_integer(fe.list.size-1) }

	function on_tick(ttime) {
		if (object.video_duration == 0) { // Object is an image or not drawn yet
			if (fe.layout.time >= transition_time + delay) next()
		}
	 	else if (fe.layout.time >= transition_time + object.video_duration) next() // Object is a video
	}

	function on_transition(ttype, var, ttime) {
		if (ttype == Transition.FromOldSelection) transition_time = fe.layout.time
		return false
	}
}
