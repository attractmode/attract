/****************************************
*
*  OSD
*  author: Keil Miller Jr
*
****************************************/

class OSD {
	effect = {
		delay = null,
		duration = null,
		type = [
			"collapse",
			"compress",
			"disappear",
			"fade",
			"roll down",
			"roll up",
		],
	}
	object = null
	properties = {
		x = null,
		y = null,
		width = null,
		height = null,
		alpha = null,
	}
	transition_time = 0
	velocity = 0

	constructor(obj, t = "collapse", de = 3000, du = 200) {
		// Object and Properties
		object = obj
		try {
			properties.x = object.x
			properties.y = object.y
			properties.width = object.width
			properties.height = object.height
			properties.alpha = object.alpha
		}
		catch (e) {
			log.error("Improper object.")
			return
		}

		// Effect Type
		try {
			assert(effect.type.find(strip(t.tolower())) != null)
			effect.type = strip(t.tolower())
		}
		catch (e) {
			log.warn("Improper effect type, switching to default value.")
			effect.type = "collapse"
		}

		// Effect Delay
		try {
			effect.delay <- de.tointeger()
			assert(effect.delay >= 1)
		}
		catch (e) {
			log.warn("Improper effect delay time, switching to default value.")
			effect.delay <- 3000
		}

		// Effect Duration
		try {
			effect.duration = du.tointeger()
			assert(effect.duration >= 1)
		}
		catch (e) {
			log.warn("Improper effect duration time, switching to default value.")
			effect.duration = 200
		}

		fe.add_ticks_callback(this, "on_tick")
		fe.add_transition_callback(this, "on_transition")
	}

	function collapse() {
		object.height = fabs(object.height - properties.height / velocity)
		object.y = fabs(object.y + properties.height / 2 / velocity)
	}

	function compress() {
		object.height = fabs(object.height - properties.height / velocity)
		object.y = fabs(object.y + properties.height / 2 / velocity)
		object.width = fabs(object.width - properties.width / velocity)
		object.x = fabs(object.x + properties.width / 2 / velocity)
	}

	function disappear() { object.visible = false }

	function fade() { object.alpha = fabs(object.alpha - properties.alpha / velocity) }

	function on_tick(ttime) {
		velocity = effect.duration / delta.get()
		if (fe.layout.time >= transition_time + effect.delay && fe.layout.time <= transition_time + effect.delay + effect.duration) {
			switch (effect.type) {
				case "collapse":
					collapse()
					break
				case "compress":
					compress()
					break
				case "fade":
					fade()
					break
				case "roll down":
					roll_down()
					break
				case "roll up":
					roll_up()
					break
				case "disappear":
				default:
					disappear()
					break
			}
		}
		if (fe.layout.time > transition_time + effect.delay + effect.duration) disappear() // Disappear if effect did not finish
	}

	function on_transition(ttype, var, ttime) {
		if ((ttype == Transition.FromOldSelection) || (ttype == Transition.ToNewList)) {
			restore()
			transition_time = fe.layout.time
		}
		return false
	}

	function restore() {
		object.x = properties.x
		object.y = properties.y
		object.width = properties.width
		object.height = properties.height
		object.visible = true
		object.alpha = properties.alpha
	}

	function roll_down() {
		object.height = fabs(object.height - properties.height / velocity)
		object.y = fabs(object.y + properties.height / velocity)
	}

	function roll_up() {
		object.height = fabs(object.height - properties.height / velocity)
		object.y = fabs(object.y - properties.height / velocity)
	}
}
