/****************************************
*
*  Common Module / Properties
*  author: Keil Miller Jr
*  mentions:
*    liquid8d for set_properties
*
****************************************/

::match_aspect <- function(opts = {}) {
	// Validations
	try {
		assert(typeof opts == "table")
		assert(typeof opts.aspect_height == "float" || typeof opts.aspect_height == "integer")
		assert(typeof opts.aspect_width == "float" || typeof opts.aspect_width == "integer")
		assert("height" in opts && "width" in opts == false || "height" in opts == false && "width" in opts)
		if ("height" in opts) assert(typeof opts.height == "float" || typeof opts.height == "integer")
		if ("width" in opts) assert(typeof opts.width == "float" || typeof opts.width == "integer")
	}
	catch(e) return

	// Return height
	if ("width" in opts) return (opts.aspect_height * opts.width) / opts.aspect_width

	// Return width
	if ("height" in opts) return (opts.aspect_width * opts.height) / opts.aspect_height
}

::set_properties <- function(object, properties) {
	foreach(key, value in properties) {
		try {
			switch (key) {
				case "rgb":
					object.set_rgb(value[0], value[1], value[2])
					if (value.len() > 3) object.alpha = value[3]
					break
				case "bg_rgb":
					object.set_bg_rgb(value[0], value[1], value[2])
					if (value.len() > 3) object.bg_alpha = value[3]
					break
				case "sel_rgb":
					object.set_sel_rgb(value[0], value[1], value[2])
					if (value.len() > 3) object.sel_alpha = value[3]
					break
				case "selbg_rgb":
					object.set_selbg_rgb(value[0], value[1], value[2])
					if (value.len() > 3) object.selbg_alpha = value[3]
					break
				default:
					object[key] = value
			}
		}
		catch(e) {}
	}
}

::shade_object <- function(object, percent) {
	if (percent > 255 || percent < 0) return false
	try {
		object.red = (percent / 100.0) * 255.0
		object.green = (percent / 100.0) * 255.0
		object.blue = (percent / 100.0) * 255.0
	}
	catch(e) return false
}

// Aliases

::set_props <- set_properties
::shade_obj <- shade_object
