/****************************************
*
*  Common Module / Values
*  author: Keil Miller Jr
*
****************************************/

::explode <- function(string, separator = ",") {
	if (typeof string != "string" || typeof separator != "string") return false
	return split(string, separator)
}

::implode <- function(array, separator = ",") {
	if (typeof array != "array" || typeof separator != "string") return false
	local output = ""
	foreach (value in array){
		output += strip(value) + separator
	}
	return output
}

::in_range <- function(val, low, high) {
	if (val >= low && val <= high) return true
	return false
}

::mean <- function(array) {
	try {
		assert(typeof array == "array")
		foreach (value in array) {
			assert(typeof value == "integer" || typeof value == "float")
		}
	}
	catch(e) return false

	local sum = 0.0
	foreach (value in array) sum += value
	return sum / array.len()
}

::median <- function(array) {
	try {
		assert(typeof array == "array")
		foreach (value in array) { assert(typeof value == "integer" || typeof value == "float") }
	}
	catch(e) return false

	array.sort()
	if (array.len() % 2 == 0) return (array[array.len() / 2 - 1] + array[array.len() / 2]) / 2.0 // Return value for mean of two middle numbers
	else return array[(array.len() - 1) / 2] // Return value for middle number
}

::mode <- function(array) {
	try {
		assert(typeof array == "array")
		foreach (value in array) assert(typeof value == "integer" || typeof value == "float")
	}
	catch(e) return false

	local frequency = {}
	foreach (value in array) value in frequency ? frequency[value] += 1 : frequency[value] <- 1
	local maximum = []
	foreach (key, value in frequency) {
		if (maximum.len() == 0 || value > frequency[maximum[0]]) maximum = [key] // maximum array is empty OR value is greater than the frequency of the first key in maximum
		else if (value == frequency[maximum[0]] && maximum.find(key) == null) maximum.push(key) // value is equal to the frequency of the first key in maximum AND the key does not exist in maximum
	}
	return maximum
}

::percentage <- function(opts) {
		if (typeof(opts) != "table") return

		// Aliases
		// percent == p
		if ("percent" in opts && !("p" in opts))
			opts.p <- opts.percent

		// part == x
		if ("part" in opts && !("x" in opts))
			opts.x <- opts.part

		// whole == y
		if ("whole" in opts && !("y" in opts))
			opts.y <- opts.whole

		// Validations
		if ("p" in opts)
			if (opts.p < 0) return

		if ("x" in opts)
			if (opts.x < 0) return

		if ("y" in opts)
			if (opts.y < 0) return

		if ("p" in opts && "x" in opts && "y" in opts) return

		// Formulas
		// part = whole * percent / 100
		if ("p" in opts && "y" in opts) return (opts.y.tofloat() * opts.p.tofloat()) / 100.0

		// percent = part * 100 / whole
		if ("x" in opts && "y" in opts) return (opts.x.tofloat() * 100.0) / opts.y.tofloat()

		// whole = part * 100 / percent
		if ("p" in opts && "x" in opts) return (opts.x.tofloat() * 100.0) / opts.p.tofloat()

		// return, no opts matched
		return
	}

::random_boolean <- function() {
	return random_integer(1) == 1
}

::random_integer <- function(max) {
	// seed based on the fastrand32.py module from the python library PyRandLib
	// https://github.com/schmouk/PyRandLib/blob/master/PyRandLib/fastrand32.py
	local seedTime = time() * 1000
	srand(((seedTime & 0xff000000) >> 24) + ((seedTime & 0x00ff0000) >>  8) + ((seedTime & 0x0000ff00) <<  8) + ((seedTime & 0x000000ff) << 24))
	return ((1.0 * rand() / RAND_MAX) * (max + 1)).tointeger()
}

::to_boolean <- function(x) {
	local data = [
		"locked",
		"on",
		"positive",
		"yes",
	]

	switch(typeof x) {
		case "bool": // Squirrel has a boolean type(bool) however like C++ it considers null, 0(integer) and 0.0(float) as false, any other value is considered true.
			return x
			break
		case "string":
			if (data.find(x.tolower()) != null) return true
			break
		default:
			return false
			break
	}
}

// Aliases

::per <- percentage
::rand_bool <- random_boolean
::rand_int <- random_integer
::to_bool <- to_boolean
