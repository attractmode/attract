/****************************************
*
*  Common Module / Display
*  author: Keil Miller Jr
*
****************************************/

::is_layout_vertical <- function() {
	return (fe.layout.base_rotation + fe.layout.toggle_rotation) % 4 == (1 || 3)
}

::is_widescreen <- function() {
	return fe.layout.width.tofloat() / fe.layout.height.tofloat() > 4.0 / 3.0
}

::split_resolution <- function(resolution) {
	local delimiters = [
		":",
		";",
		"x",
	]

	// Validations
	try {
		assert(typeof resolution == "string")
	}
	catch(e) return

	// Return array
	for (local array, i = 0; i < delimiters.len(); i++) {
		array = split(resolution, delimiters[i])

		if (array.len() == 2) return array
	}
}

// Aliases

::is_layout_vert <- is_layout_vertical
::split_res <- split_resolution
