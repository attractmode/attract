/****************************************
*
*  Common Module / Delta
*  author: Keil Miller Jr
*  mentions: Radek Dutkiewicz (oomek)
*
****************************************/

class Delta {
	// Usage
	// offset / ( transition_time / frame_time )
	// 100 / ( 2000 / 16.667 ) = 0.8333 px per frame
	delay = null
	frame = 0
	object = null
	poll = []
	previous_time = 0
	samples = null

	constructor(s = 10, d = 5) {
		delay = d
		object = fe.add_image(fe.module_dir + "pixel.png", 0, fe.layout.height, 1, 1)
		samples = s

		fe.add_transition_callback(this, "on_transition")
	}

	// Public

	function get() {
		if (poll.len() != samples) return false // Return false, start of layout and poll not complete
		local sum = 0
		foreach (value in poll) sum += value
		return sum.tofloat() / poll.len() // Return the mean of all deltas in poll
	}

	// Private

	function sample(ttime) {
		frame++ // Update frame count
		if (frame <= delay + samples) { // Have not exceeded necesary frames
			(object.x == 0) ? object.x++ : object.x-- // Draw on each frame by moving object
			if (frame > delay) poll.append(ttime - previous_time) // Add a delta to poll
		}
		previous_time = ttime // Completed sample, overwrite previous_time
	}

	function on_transition(ttype, var, ttime) {
		if (ttype == Transition.StartLayout && poll.len() < samples) { // Start of layout and poll not complete
			sample(ttime)
			return true // Redraw
		}
		else return false // Transition effect is done
	}
}

::delta <- Delta()
